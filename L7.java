var a = [1, 2, 2, 4, 10, 8, 6, 8, 9, 8, 6, 6]

count = Object.create(null);

for (var q=0; q<a.length; ++q) {
  count[a[q]] = ~~count[a[q]] + 1
}

a.sort(function (x, y) {
  return count[y]-count[x] || x-y
})

console.log(a.join(' '))